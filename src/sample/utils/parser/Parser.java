package sample.utils.parser;

import javax.swing.plaf.nimbus.State;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    private String restSource;
    private String lastToken;
    private States currentState = States.Expression;
    private int priority;
    private Stack<Node> operations = new Stack<>();
    private Map<String, Double> variables = new HashMap<>();

    private ArrayList<Node> result = new ArrayList<Node>();

    private class Node {
        Signals signal;
        String token;

        public Node(Signals signal, String token) {
            this.signal = signal;
            this.token = token;
        }
    }

    private enum States {
        Expression,
        Operation,
        Operand,
        Finish,
        Error
    }

    private enum Signals {
        Space,
        Number,
        BinaryOperation,
        UnaryOperation,
        Function,
        OpenParenthesis,
        CloseParenthesis,
        EndLine,
        Variable,
        Unknown
    }

    private static Map<Pattern, Signals> PatternSignal = new HashMap<Pattern, Signals>() {{
        put(Pattern.compile("\\s+"), Signals.Space);
        put(Pattern.compile("[0-9]*(?:\\.?[0-9]+)(?:[eE][-+]?[0-9]+)?"), Signals.Number);
        put(Pattern.compile("[+*/]"), Signals.BinaryOperation);
        put(Pattern.compile("-"), Signals.UnaryOperation);
        put(Pattern.compile("sin|cos|tan|ln|ctg|sinh|cosh|tanh|lg|abs|sqrt"), Signals.Function);
        put(Pattern.compile("\\("), Signals.OpenParenthesis);
        put(Pattern.compile("\\)"), Signals.CloseParenthesis);
        put(Pattern.compile("\n|$"), Signals.EndLine);
        put(Pattern.compile("[a-zA-Z][a-zA-Z0-9]*"), Signals.Variable);
    }};


    private Signals getSignal() {
        for (Map.Entry<Pattern, Signals> entry : PatternSignal.entrySet()) {
            Matcher matcher = entry.getKey().matcher(restSource);

            if (matcher.lookingAt()) {
                lastToken = matcher.group();
                restSource = restSource.substring(matcher.end());

                return entry.getValue();
            }
        }

        return Signals.Unknown;
    }

    public Parser(String expression) {
        variables.put("e", Math.E);
        variables.put("pi", Math.PI);

        restSource = expression;

        parse();
    }

    public void setVariable(String variable, double value) {
        variables.put(variable, value);
    }

    private void displace() {
        while (!operations.empty() && operations.peek().signal != Signals.OpenParenthesis) {
            result.add(operations.pop());
        }
    }

    private static int getPriority(char token) {
        if (token == '+') return 1;
        if (token == '-') return 1;
        if (token == '*') return 2;
        if (token == '/') return 2;
        return 0; // Возвращаем 0 если токен - это не бинарная операция (например ")") WTF???
    }

    public void parse() {
        while (restSource.length() > 0) {
            Signals signal = getSignal();
            Cell cell = TrackTable[currentState.ordinal()][signal.ordinal()];
            cell.handler.handle(cell.state, signal);
        }

        while (!operations.empty()) {
            displace();
        }
    }

    public void read(States nextState, Signals currentSignal) {
        this.currentState = nextState;
        result.add(new Node(currentSignal, lastToken));
    }

    public void readSpaces(States nextState, Signals currentSignal) {
        this.currentState = nextState;
    }

    public void ReadUnaryOperation(States state, Signals currentSignal) {
        this.currentState = state;
        operations.push(new Node(Signals.UnaryOperation, "#"));
    }

    public void ReadBinaryOperation(States state, Signals currentSignal) {
        this.currentState = state;

        int newPriority = getPriority(lastToken.charAt(0));
        if (newPriority < priority) {
            displace();
        }
        priority = newPriority;
        operations.push(new Node(currentSignal, lastToken));
    }

    public void ReadCloseParenthesis(States state) {
        this.currentState = state;

        displace();

        if (!operations.empty()) {
            operations.pop();
        } else {
            throw new RuntimeException("Extra close parenthesis");
        }

        priority = operations.empty() ? 0 : getPriority(operations.peek().token.charAt(0));
    }

    public void ReadVariable(States nextState, Signals currentSignal) {
        this.currentState = nextState;

        result.add(new Node(currentSignal, lastToken));

        if (!variables.containsKey(lastToken)) {
            setVariable(lastToken, 1.0);
        }
    }

    public double execute() {
        Stack<Double> operands = new Stack<>();

        for (Node node : result) {
            switch (node.signal) {
                case Number:
                    operands.push(Double.parseDouble(node.token));
                    break;
                case BinaryOperation:
                    Double second = operands.pop();
                    Double first = operands.pop();
                    if (node.token.equals("*"))
                        operands.push(first * second);
                    else if (node.token.equals("/"))
                        operands.push(first / second);
                    else if (node.token.equals("+"))
                        operands.push(first + second);
                    else if (node.token.equals("-"))
                        operands.push(first - second);
                    break;
                case UnaryOperation:
                    operands.push(0 - operands.pop());
                    break;
                case Function:
                    operands.push(processFunction(node.token, operands.pop()));
                    break;
                case Variable:
                    operands.push(variables.get(node.token));
                    break;
                default:
                    throw new RuntimeException("token " + node.token + " is not defined");
            }
        }

        return operands.pop();
    }

    private double processFunction(String func, Double value) {
        switch (func) {
            case "sin":
                return Math.sin(value);
            case "cos":
                return Math.cos(value);
            case "tan":
                return Math.tan(value);
            case "log":
                return Math.log(value);
            case "log10":
                return Math.log10(value);
            case "cot":
                return 1 / Math.tan(value);
            case "sinh":
                return Math.sinh(value);
            case "cosh":
                return Math.cosh(value);
            case "tanh":
                return Math.tanh(value);
            case "abs":
                return Math.abs(value);
            case "sqrt":
                return Math.sqrt(value);
            default:
                throw new RuntimeException("function '" + func + "' is not defined");
        }
    }

    public void HandleError(States nextState, Signals currentSignal) {
        this.currentState = nextState;
        throw new RuntimeException("Unknown syntax expression");
    }

    @FunctionalInterface
    private interface ParserHandler {
        void handle(States nextState, Signals currentSignal);
    }

    private class Cell {
        States state;
        ParserHandler handler;

        Cell(States state, ParserHandler parserHandler) {
            this.state = state;
            this.handler = parserHandler;
        }
    }

    Cell[][] TrackTable = {
            { new Cell(States.Expression, this::readSpaces), new Cell(States.Operation, this::read), new Cell(States.Error, this::HandleError),
                    new Cell(States.Operand, this::ReadUnaryOperation), new Cell(/*Operation*/States.Expression, this::read), new Cell(States.Expression, this::read),
                    new Cell(States.Operation, this::read), new Cell(States.Error, this::HandleError), new Cell(States.Operation, this::ReadVariable) },

            { new Cell(States.Operation, this::readSpaces), new Cell(States.Error, this::HandleError), new Cell(States.Operand, this::ReadBinaryOperation),
                    new Cell(States.Operand, this::ReadBinaryOperation), new Cell(States.Error, this::HandleError), new Cell(States.Error, this::HandleError),
                    new Cell(States.Operation, this::read), new Cell(States.Finish, this::read), new Cell(States.Error, this::HandleError) },

            { new Cell(States.Operand, this::readSpaces), new Cell(States.Operation, this::read), new Cell(States.Error, this::HandleError),
                    new Cell(States.Error, this::HandleError), new Cell(/*Operation*/States.Expression, this::read), new Cell(States.Expression, this::read),
                    new Cell(States.Error, this::HandleError), new Cell(States.Error, this::HandleError), new Cell(States.Operation, this::ReadVariable ) }
    };
}
